/*
 * LCD.c
 *
 *  Created on: 26 ����. 2018 �.
 *      Author: AnokhinaIrina
 */

#include"LCD.h"
#include"i2c.h"

uint8_t buf[1] = {0};
extern I2C_HandleTypeDef hi2c1;


void delay_micro(uint32_t micros)
{
	 micros *= (SystemCoreClock / 1000000 / 5 );
}

void LCD_WriteByte(uint8_t dt)
{
	buf[0] = dt;
	HAL_I2C_Master_Transmit(&hi2c1, (uint16_t) 0x7E, buf, 1, 1000);

}

void send_halfbyte(uint8_t c)
{
	c <<= 4;
	port_LCD &= 0x0F;
	e_set();

	port_LCD |= c;
	delay_micro(50);
	 LCD_WriteByte(port_LCD);
	 e_reset();
	 HAL_Delay(1);
	 delay_micro(50);
}

 void send_byte(uint8_t c, uint8_t mode)
 {
	 if(mode)
	 {
		 rs_set();
	 }
	 else
	 {
		 rs_reset();
	 }
	 uint8_t hc = 0;
	 hc = c >> 4;
	 send_halfbyte(hc);
	 send_halfbyte(c);
 }

void LCD_ini()
{
	HAL_Delay(50);
	rs_reset();
	send_halfbyte(0x03);
	HAL_Delay(4);
	send_halfbyte(0x03);
	HAL_Delay(4);
	send_halfbyte(0x03);
	HAL_Delay(1);
	send_halfbyte(0x02);
	send_byte(0x28, 0);
	HAL_Delay(1);
	send_byte(0x28, 0);
		HAL_Delay(1);
	send_byte(0x0C, 0);
		HAL_Delay(1);
	send_byte(0x01, 0);
	HAL_Delay(2);
	send_byte(0x06, 0);
	HAL_Delay(1);
	send_byte(0x02, 0);
	//send_byte(0x80, 0);
	HAL_Delay(1);
set_led();
	set_write();
}

void LCD_Clear()
{
	send_byte(0x01, 0);
	HAL_Delay(2);
	//delay_micro(1500);
}

void LCD_Send_Char(char ch)
{
send_byte((uint8_t)ch, 1);
}

void LCD_String(char *s)
{
	 while(*s)LCD_Send_Char(*s++);
}

void LCD_Set_Position(uint8_t x, uint8_t y)
{
	switch(y)
	{
	case 0:
		send_byte(x|0x80, 0);
		HAL_Delay(1);
		break;
	case 1:
		send_byte((x + 0x40)|0x80, 0);
				HAL_Delay(1);
				break;
default:
	break;

	}
}
