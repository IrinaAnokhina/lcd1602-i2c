/*
 * LCD.h
 *
 *  Created on: 26 ����. 2018 �.
 *      Author: AnokhinaIrina
 */

#ifndef LCD_H_
#define LCD_H_

#include"stdint.h"

uint8_t port_LCD;
#define e_set() LCD_WriteByte(port_LCD |= 0x04);
#define e_reset() LCD_WriteByte(port_LCD &= ~0x04);
#define rs_set() LCD_WriteByte(port_LCD |= 0x01);
#define rs_reset() LCD_WriteByte(port_LCD &= ~0x01);
#define set_led() LCD_WriteByte(port_LCD |= 0x08);
#define set_write() LCD_WriteByte(port_LCD &= ~0x02);
#define reset_write() LCD_WriteByte(port_LCD |= 0x02);

/*void e_set();
void e_reset();*/
void send_byte(uint8_t c, uint8_t mode);
void LCD_ini(void);
void LCD_Clear(void);
void LCD_Send_Char(char ch);
void LCD_String(char* s);
void LCD_Set_Position(uint8_t x, uint8_t y);


#endif /* LCD_H_ */
